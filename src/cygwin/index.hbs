{{>head title="Running bash script on Windows using Cygwin" meta_description="" meta_keywords="" meta_author="Black Quadrant Technologies"}}
<body>
	{{>header}}
    {{>aside}}
	<section class="page-main">
		<div class="page-content">
            <div class="page-header">
                <h1 class="display-4">Running bash script on Windows using Cygwin</h1>
                <p class="lead">Bash scripting is an extremely useful and powerful part of system administration and development.</p>
                <p class="lead">Cygwin is a POSIX-compatible environment that runs natively on Microsoft Windows. Its goal is to allow programs of Unix-like systems to be recompiled and run natively on Windows with minimal source code modifications by providing them with the same underlying POSIX API they would expect in those systems.</p>
            </div>
            <br/>
            <div class="page-md">
                <p>Bash is a Unix shell, which is a command line interface (CLI) for interacting with an operating system (OS). Any command that you can run from the command line can be used in a bash script. Scripts are used to run a series of commands.</p>
                <p>Bash is available by default on Linux and macOS operating systems.</p>
                <div id="create-bin-directory">
                    <h4>Create a bin directory</h4>
                    <p>The first step is to create a bin directory. bin is the standard naming convention of a subdirectory that contains executable programs.</p>
                    <p>You can make sure you're in the main user directory by navigating to ~ (which is a shortcut for current user home directory). This will also be the default directory Terminal always opens in. Typing pwd will confirm the location, as well.</p>
                    <p>Create bin in that folder, or wherever you want your bash scripts to live.</p>
                    <pre><code>cd ~      # this takes us to /Users/your_username</code></pre>
                    <pre><code>mkdir bin # this creates /Users/your_username/bin</code></pre>
                </div>
                <div id="export-bin-directory">
                    <h4>Export your bin directory to the PATH</h4>
                    <p>Open .bash_profile, which will be located at /Users/your_username/.bash_profile, and add this line to the file. If .bash_profile doesn't exist, create it.</p>
                    <pre><code>export PATH=$PATH:/Users/your_username/bin</code></pre>
                    <p>If you don't see hidden files and directories, or those that begin with a ., press Command + SHIFT + .. If Terminal.app is open, quit and reopen it so the PATH gets updated.</p>
                </div>
                <div id="create-script-file">
                    <h4>Create a script file and make it executable</h4>
                    <p>Go to your bin folder located in /Users/your_username.</p>
                    <pre><code>cd bin</code></pre>
                    <p>Create a file called hello-world (no extension) in this folder.</p>
                    <pre><code>touch hello-world</code></pre>
                    <p>Open the file in your text editor of choice and type the following.</p>
                    <pre><code>#!/bin/bash</code></pre>
                    <p>A bash script must always begin with #!/bin/bash to signify that the script should run with bash as opposed to any other shell. This is called a "shebang". You can confirm where the bash interpreter is located with which bash.</p>
                    <pre><code>which bash</code></pre>
                    <p>As is tradition, we'll make a "Hello, World!" example to get this working</p>
                    <pre><code>#!/bin/bash</code></pre>
                    <pre><code>echo Hello, World!</code></pre>
                    <p>We have to make it an executable file by changing the permissions.</p>
                    <pre><code>chmod u+x hello-world</code></pre>
                    <p>Now, you can run the file in the terminal.</p>
                    <pre><code>hello-world</pre></code>
                    <p>It will output the contents of the echo. You can also run this script from anywhere on the computer, not just in the bin directory.</p>
                </div>
                <div id="variables">
                    <h4>Variables</h4>
                    <p>A variable is declared without a $, but has a $ when invoked. Let's edit our hello-world example to use a variable for the entity being greeted, which is World.</p>
                    <pre><code>#!/bin/bash</code></pre>
                    <pre><code>who="World"</code></pre>
                    <pre><code>echo Hello, $who!</code></pre>
                    <p>Note that who = "World" is not valid - there must not be a space between variable and value.</p>
                </div>
                <div id="reading">
                    <h4>Reading</h4>
                    <p>We declared a variable in the last example, but we can also have the user set the value of a variable dynamically. For example, instead of just having the script say Hello, World!, we can make it ask for the name of the person calling the script, then output that name. We'll do this using the read command.</p>
                    <pre><code>#!/bin/bash</code></pre>
                    <pre><code>echo Who are you?</code></pre>
                    <pre><code>read who</code></pre>
                    <pre><code>echo Hello, $who!</code></pre>
                </div>
                <div id="conditionals">
                    <h4>Conditionals</h4>
                    <p>if statements use the if, then, else, and fi keywords. The condition goes in square brackets.</p>
                    <pre><code>#!/bin/bash</code></pre>
                    <pre><code>echo How old are you?</code></pre>
                    <pre><code>read age</code></pre>
                    <pre><code>if [ "$age" -gt 20 ]</code></pre>
                    <pre><code>then</code></pre>
                    <pre><code>echo You can drink.</code></pre>
                    <pre><code>else</code></pre>
                    <pre><code>echo You are too young to drink.</code></pre>
                    <pre><code>fi</code></pre>
                    <p>Operators are slightly different in bash than what you might be used to.</p>
                    <table>
                        <tr>
                            <th>Bash Operator</th>
                            <th>Operator</th>
                            <th>Description</th>
                        </tr>
                        <tr>
                            <td>-eq</td>
                            <td>==</td>
                            <td>Equal</td>
                        </tr>
                        <tr>
                            <td>-ne</td>
                            <td>!=</td>
                            <td>Not equal</td>
                        </tr>
                        <tr>
                            <td>-gt</td>
                            <td>></td>
                            <td>Greater than</td>
                        </tr>
                        <tr>
                            <td>-ge</td>
                            <td>>=</td>
                            <td>Greater than or equal</td>
                        </tr>
                        <tr>
                            <td>-lt</td>
                            <td><</td>
                            <td>Less than</td>
                        </tr>
                        <tr>
                            <td>-le</td>
                            <td><=</td>
                            <td>Less than or equal</td>
                        </tr>
                        <tr>
                            <td>-z</td>
                            <td>== null</td>
                            <td>Is null</td>
                        </tr>
                    </table>
                </div>
                <div id="looping">
                    <h4>Looping</h4>
                    <p>Bash uses for, while, and until loops. In this example, I'll use the for...in loop to get all the files in a directory and list them.</p>
                    <pre><code>#!/bin/bash</code></pre>
                    <pre><code>FILES=/Users/your_username/directory_name/*</code></pre>
                    <pre><code>for file in $FILES</code></pre>
                    <pre><code>do</code></pre>
                    <pre><code>echo $(basename $file)</code></pre>
                    <pre><code>done</code></pre>
                </div>
                <div id="git-deploy-example">
                    <h4>Git Deploy Example Script</h4>
                    <p>As mentioned previously, a bash script can use any commands you can use on the command line. An example of a script you might make for yourself is the one below, where the user is prompted for a git commit message and the process of adding, committing, and pushing to origin is all done with a single git-deploy command.</p>
                    <pre><code>#!/bin/bash</code></pre>
                    <pre><code>read -r -p 'Commit message: ' desc  # prompt user for commit message</code></pre>
                    <pre><code>git add .                           # track all files</code></pre>
                    <pre><code>git add -u                          # track deletes</code></pre>
                    <pre><code>git commit -m "$desc"               # commit with message</code></pre>
                    <pre><code>git push origin master              # push to origin</code></pre>
                    <p>Then just run the command.</p>
                    <pre><code>$ git-deploy</code></pre>
                </div>
                <div id="install-cygwin">
                    <h4>Installation</h4>
                    <p>This file is a Windows-based package manager for Cygwin. By default, it will download and install the latest minimal install of Cygwin, but on future launches, it will also enable you to add new packages and update your existing Cygwin software. So after you install Cygwin do not delete or lose the location of setup.exe!</p>
                    <ol>
                        <li>Visit http://cygwin.com/install.html and download.</li>
                        <li>Run application from your local harddrive.</li>
                        <li>Choose next on first screen.</li>
                        <li>Select "Install from Internet" and click next.</li>
                        <li>Enter preferred installation (Root) directory and click next.</li>
                        <li>Enter a temporary installation directory and click next.</li>
                        <li>Select "Direct Connection" and click next.</li>
                        <li>Select a download site and click next.</li>
                        <li>Select the packages you want to install and click next.</li>
                        <li>Once the installation is complete, click Finish and continue with the Setup section.</li>
                    </ol>
                </div>
                <div id="starting-cygwin">
                    <h4>Working with Directories</h4>
                    <p>Open your Cygwin Console by clicking: Start>All Programs>Cygwin>Cygwin Bash Shell.</p>
                </div>
                <div id="working-with-directories">
                    <h4>Working with Directories</h4>
                    <p>When you start a Cygwin Console, you are automatically sent to your Cygwin home directory - which usually corresponds to your Windows username. All Windows users on your system should have a home directory with a Windows path of: c:\cygwin\home\[Windows Username]</p>
                    <p>To find out what your home directory name is type the 'pwd' (i.e. print working directory) command  in a newly opened Cygwin Console.</p>
                    <pre><code>$pwd</code></pre>
                    <p class="tip">
                        Cywin uses the Linux standard approach to naming directory paths - which uses the forward slash "/" to separate directories, rather than the Windows approach with uses the backward slash "\" to separate directories.
                    </p>
                    <p class="tip">
                        Cygwin does not use the [drive letter + ":"] notation to denote your root directory on a hard disk - i.e. there is no "c:" drive in Cygwin.  The Cygwin root directory is denoted by a single forward slash "/".
                    </p>
                </div>
                <div id="working-with-directories">
                    <h4>Working with Directories</h4>
                    <p>When you start a Cygwin Console, you are automatically sent to your Cygwin home directory - which usually corresponds to your Windows username. All Windows users on your system should have a home directory with a Windows path of: c:\cygwin\home\[Windows Username]</p>
                    <p>To find out what your home directory name is type the 'pwd' (i.e. print working directory) command  in a newly opened Cygwin Console.</p>
                    <pre><code>$pwd</code></pre>
                    <p class="tip">
                        Cywin uses the Linux standard approach to naming directory paths - which uses the forward slash "/" to separate directories, rather than the Windows approach with uses the backward slash "\" to separate directories.
                    </p>
                    <p class="tip">
                        Cygwin does not use the [drive letter + ":"] notation to denote your root directory on a hard disk - i.e. there is no "c:" drive in Cygwin.  The Cygwin root directory is denoted by a single forward slash "/".
                    </p>
                </div>
                <div id="directory-structure">
                    <h4>Directory Structure</h4>
                    <p>Cygwin attempts to duplicate the directory structure of a Linux System within the "c:\cygwin" folder:</p>
                    <table>
                        <tr>
                            <th>Windows location</th>
                            <th>Cygwin Console</th>
                        </tr>
                        <tr>
                            <td>c:\cygwin\bin</td>
                            <td>/bin</td>
                        </tr>
                        <tr>
                            <td>c:\cygwin\etc</td>
                            <td>/etc</td>
                        </tr>
                        <tr>
                            <td>c:\cygwin\home</td>
                            <td>/home</td>
                        </tr>
                        <tr>
                            <td>c:\cygwin\home\administrator</td>
                            <td>/home/administrator</td>
                        </tr>
                        <tr>
                            <td>c:\cygwin\lib</td>
                            <td>/lib</td>
                        </tr>
                        <tr>
                            <td>c:\cygwin\tmp</td>
                            <td>/tmp</td>
                        </tr>
                        <tr>
                            <td>c:\cygwin\usr</td>
                            <td>/usr</td>
                        </tr>
                        <tr>
                            <td>c:\cygwin\var</td>
                            <td>/var</td>
                        </tr>
                        <tr>
                            <td>c:\cygwin</td>
                            <td>/</td>
                        </tr>
                    </table>
                </div>
                <div id="creating-files">
                    <h4>Creating Files</h4>
                    <p>When creating directories and files in your home directory in Cygwin, remember that you can use Windows Explorer to create a file, and Windows Wordpad or any editor of your choice to edit it (see below for the Cygwin commands to do this).</p>
                    <p class="tip">
                        <strong>Note:</strong> If you create a file directly in WordPad, when you save it WordPad will try to append a '.txt' or '.rtf' suffix to your file name.
                    </p>
                </div>
                <div id="cutting-pasting">
                    <h4>Cutting and Pasting Content from Windows to Cygwin</h4>
                    <p>From the Windows application:</p>
                    <ul>
                        <li>highlight the text to be copied;</li>
                        <li>right-click and select copy from right-click menu (or hit ctrl-c key combination);</li>
                    </ul>
                    <p>go to Cygwin window:</p>
                    <ul>
                        <li>right-click Cygwin window header</li>
                        <li>select Edit>Paste from the right-click menu</li>
                    </ul>    
                </div>
                <div id="common-commands">
                    <h4>Common Cygwin commands:</h4>
                    <h5>Working with Files</h5>
                    <pre><code>cp <filename> <new filename></code></pre>
                    <p>copy - Make a copy of a file</p>
                    <pre><code>cp -R <directory> <new directory></code></pre>
                    <p>Make a copy of a directory</p>
                    <pre><code>mv <filename> <new filename></code></pre>
                    <p>move - Move or rename a file</p>
                    <pre><code>rm <filename></code></pre>
                    <p>remove - Delete a file</p>
                    <h5>Working with Directories</h5>
                    <pre><code>cd <directory></code></pre>
                    <p>change directory - Change to the directory specified</p>
                    <pre><code>ls</code></pre>
                    <p>List - Lists the files in the current directory</p>  
                    <pre><code>ls -l</code></pre>
                    <p>Lists the files and their attributes</p>
                    <pre><code>mkdir <new directory name></code></pre>
                    <p>make directory - Create a new directory</p>
                    <pre><code>pwd</code></pre>
                    <p>Path of working directory - tells you what directory you are in</p>
                    <h5>Archiving/Extracting Files and Directories</h5>
                    <pre><code>tar -zcvf  <filename> <directory>  # create  gzipped  tar  archive  of  <directory></code></pre>
                    <ul>
                        <li>-z - filter the archive through gzip</li>
                        <li>-c -  create a new archive</li>
                        <li>-v - verbosely list files processed</li>
                        <li>-f - use archive file</li>
                    </ul>
                    <pre><code>tar -xvzf  <filename>     # extract tarred, gzipped <filename> in current directory</code></pre>
                    <ul>
                        <li>-x - extract files from an archive</li>
                        <li>-v - verbosely list files processed</li>
                        <li>-z - filter the archive through gzip</li>
                        <li>-f - use archive file</li>
                    </ul>
                    <h5>Working with File Permissions</h5>
                    <pre><code>chmod u+x <filename></code></pre>
                    <p>changes permission of the named file to executable.</p>
                    <ul>
                        <li>u - user, (this means you)</li>
                        <li>+ - adds permissions</li>
                        <li>x - executable rights</li>
                    </ul>
                    <p>You can get further details on any commands in Cygwin by using the 'info' or the 'man' command.  These provide essentially the same information, but with slightly different formatting.  To obtain detailed information on the 'mv' for example, you would type one of the following:</p>
                    <pre><code>$info mv</code></pre>
                    <pre><code>$man mv</code></pre>
                    <p>To exit info or man, type the letter 'q' at the ":" prompt.</p>
                </div>
                <div id="common-commands">
                    <h4>Common Cygwin commands:</h4>
                    <h5>Working with Files</h5>
                    
                    <p>copy - Make a copy of a file</p>
                    <pre><code>cp -R <directory> <new directory></code></pre>
                    <p>Make a copy of a directory</p>
                    <pre><code>mv <filename> <new filename></code></pre>
                    <p>move - Move or rename a file</p>
                    <pre><code>rm <filename></code></pre>
                    <p>remove - Delete a file</p>
                    <h5>Working with Directories</h5>
                    <pre><code>cd <directory></code></pre>
                    <p>change directory - Change to the directory specified</p>
                    <pre><code>ls</code></pre>
                    <p>List - Lists the files in the current directory</p>  
                    <pre><code>ls -l</code></pre>
                    <p>Lists the files and their attributes</p>
                    <pre><code>mkdir <new directory name></code></pre>
                    <p>make directory - Create a new directory</p>
                    <pre><code>pwd</code></pre>
                    <p>Path of working directory - tells you what directory you are in</p>
                </div>
            </div>
        </div>
	</section>
</body>
</html>