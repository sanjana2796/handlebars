#!/bin/bash
#   Templates
echo -e "\nComposing templates"
echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
hbs src/index.hbs --partial 'src/partials/*.hbs' --output dist/valet/ && hbs src/git/index.hbs --partial 'src/partials/*.hbs' --output dist/git/ && hbs src/handlebars/index.hbs --partial 'src/partials/*.hbs' --output dist/handlebars/ && hbs src/cygwin/index.hbs --partial 'src/partials/*.hbs' --output dist/cygwin/ && exit 1;